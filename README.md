# README #

This README is divided into five sections to provide the instructions of this experiment.

### Introduction ###

* In this paper we measure the performance of RED in the platform of GENI testbed. By using ITG, vsftpd and apache2, we try to simulate the real network environment. In this experiment, our traffic load is made of UDP, FTP and HTTP flows. There are three metrics that we are interested in: drop probability, dropping interval and buffer size. We choose different parameters to observe their influence to the performance of RED and compare the results with simple TD (Tail Drop) to evaluate RED.
* The link for original paper is:

http://ieeexplore.ieee.org/xpl/login.jsp?tp=&arnumber=766502&url=http%3A%2F%2Fieeexplore.ieee.org%2Fxpls%2Fabs_all.jsp%3Farnumber%3D766502

* It will take several hours to run this experiment and reproduce the figures and tables.


### Background ###

* RED (Random Early Detection) is a typical AQM scheme. Unlike the regular queues, RED performs TD (tail drop) in a more gradual way. Once the average queue length hits a determined threshold value (minth), packets enqueued will have a configurable chance to be dropped and this chance increases linearly to the maximum (maxp) as the average queue length reaches another threshold value (maxth). When the queue exceeds this dropping interval, all of the packets enqueued will be dropped.
* The goal of RED is to have a small queue size and to avoid the congestion which is beneficial for the interactivity and it can protect the TCP/IP traffic from dropping too many sudden packets when faced with a burst of traffic.
* The experiment can be divided into two sections. In the first section we try to reproduce the five tables in the published paper [1]. These five tables analyze the influence of three metrics (dropping probability, dropping interval and buffer size) to the performance of RED. We also try to make comparisons between RED and TD from these five tables. The second section focus on the fairness of RED vs TD, we will reproduce a figure of throughput for different TCP connections with RED and Tail Drop.

### Experiment Design ###

* The experiment topology is shown below. There are altogether 10 hosts connected to a server from a router. We only to use one host in the first section to reproduce five tables and the rest hosts will be used in the second section to observe the fairness of RED vs TD.
![topo.jpg](https://bitbucket.org/repo/8k8AKB/images/2489126858-topo.jpg)

* I try to use vsftpd (a FTP server) to generate FTP flows, use apache2 (a HTTP server) to generate HTTP flows and use D-ITG to generate UDP flows. In addition I use Traffic Control (tc) of the Linux kernel to set the parameters of RED at the router node.

### Run my experiment ###

* Use vsftpd to set up the FTP server in the host node

Here are two links about how to set up vsftpd:

 https://help.ubuntu.com/lts/serverguide/ftp-server.html

 https://www.digitalocean.com/community/tutorials/how-to-set-up-vsftpd-on-ubuntu-12-04

We have to follow the instructions in the link to allow anonymous download from FTP server. After setting up vsftpd, put seven text files of variable sizes about 100000 bytes in the "download" directory. This "download" directory can be set in the configuration file ./etc/vsftpd/vsftpd.conf.


* Use apache2 to set up a HTTP server

Here are two links about how to set up apache2:

https://help.ubuntu.com/lts/serverguide/httpd.html

https://www.digitalocean.com/community/tutorials/how-to-set-up-apache-virtual-hosts-on-ubuntu-14-04-lts

We can follow the instructions in the section link to set up a virtual host (pay attention that the link has created two hosts but we only have to set up just one host).Afters setting up apache2, I have put 12 pictures of different sizes in the "download" directory I set. So now there will a html file and 12 picture in this directory.

* Set up the RED parameters in the router node

```
#!python

sudo tc qdisc replace dev eth2 root red limit bytes min bytes max bytes avpkt bytes burst packets harddrop bandwidth 10Mbit probability chance
```

Before setting up the RED parameters, we have to use tc-tbf to set the bandwidth to 10Mbits.

Here is the instruction for setting the parameters:

http://linux.die.net/man/8/tc-red

The synopsis of tc needs us to set them in bytes, so set average packet size to be the default value of 512 bytes and make corresponding changes to the parameters settings. We set "avpkt" to 1000 bytes and use (min+min+max)/(3*avpkt) to calculate the corresponding "burst".

* Generate the FTP and HTTP traffic in the server node
 Since we set the FTP server and the HTTP server in one node. We can run two scripts at the same time to generate FTP and HTTP traffic. Run

```
#!python

sh fttpget.sh 60 & sh httpget.sh 60
```

Hint:60 is the time duration in seconds which can be set.And in the bash script httpget.sh, 10.10.2.1 is the address of the http server, this may be different in the reproducing process.

* Generate the UDP traffic in the server node at the same time.

```
#!python

ITGSend -a server -l sender.log -x receiver.log -C 1000 -u 500 524 -t 60000 -T UDP
```

Here I use a uniform distribution from 500 bytes to 524 bytes. And the packets sent per second can be modified for different UDP percentage. After UDP flow is over, observe the UDP drop rate at the server node in a new terminal. Run

```
#!python

ITGDec receiver.log
```
 

* We use iptraf to observe the throughput. After installing it at the server node, open a new terminal and note the real-time throughput. Run

```
#!python

sudo iptraf -d eth1
```

Here eth0 is the interface that you want to measure.

* Repeat the test with different parameters and observe the performance of RED.

* Set up ftp servers and http servers in the rest 9 hosts. Change the sleeping time in ftpget.sh and httpget.sh to simulate different starting time and duration intervals. In this section we use two different type of FTP files, larger file is about 100000 bytes and smaller file is 20000 bytes. Generate TCP traffic from 10 hosts at the same time and use iptraf to observe the throughput of each host.

### Results ###

![1.png](https://bitbucket.org/repo/8k8AKB/images/2979382914-1.png)
![2.png](https://bitbucket.org/repo/8k8AKB/images/2387326517-2.png)
![3.png](https://bitbucket.org/repo/8k8AKB/images/3668695053-3.png)
![4.png](https://bitbucket.org/repo/8k8AKB/images/3589095141-4.png)

 From the tables shown before, we have concluded five points about the performance of RED:

(a). Both RED and TD do not influence the TCP traffic flow since the number of TCP bytes received do not change much when we use different pairs of parameters.

(b).RED does not perform better than TD in most of the tests.

(c).When the buffer is small, the impacts of the parameters (such as maximum drop probability, dropping interval) to the performance of RED are less than that of larger buffer.

(d).When the buffer size is appropriate; we can decrease the UDP drop rate by expanding the dropping interval and decrease the maximum drop probability.


![6.png](https://bitbucket.org/repo/8k8AKB/images/3932328861-6.png)

The figure of the throughput for different TCP connections is shown below. First we can see that there is not an obvious difference between the throughput of FTP large files and FTP small files with RED. Neither RED nor Tail Drop results in “fair” goodput for all the connections when we compare the measured result with the mean throughput and RED also does not perform better than TD. In the published paper, there are some connections getting more twice (or less than the half) of the mean throughput. But there is no such condition in my result.